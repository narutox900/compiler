/* Scanner
 * @copyright (c) 2008, Hedspi, Hanoi University of Technology
 * @author Huu-Duc Nguyen
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "reader.h"
#include "charcode.h"
#include "token.h"
#include "error.h"


extern int lineNo;
extern int colNo;
extern int currentChar;

extern CharCode charCodes[];

int state = 0;


//to replace the usage of charCodes[currentChar]
CharCode currentCharCode;
/***************************************************************/

void readCharCode() {
  readChar();
  if (currentChar >= 0) {
    currentCharCode = charCodes[currentChar];
  } else {
    currentCharCode = CHAR_UNKNOWN;
  }
}

void skipBlank() {
  while (currentCharCode == CHAR_SPACE && state == 1) {
    readCharCode();
  }
}


//skip comment if *) is met, else raise an error
void skipComment() {
  while (1) {
    switch (state) {
    case 3: 
      if (currentCharCode == CHAR_TIMES) {
        readCharCode();
        state = 4;
      } else if (currentChar == EOF) {
        state = 40;
      } else {
        state = 3;
        readCharCode();
      }
      break;
    case 4:
      if (currentCharCode == CHAR_RPAR) {
        readCharCode();
        state = 5;
      } else if (currentCharCode == CHAR_TIMES) {
        readCharCode();
        state = 4;
      } else if (currentChar == EOF) {
        state = 40;
      } else {
        readCharCode();
        state = 3;
      }
      break;
    case 5:
      state = 0;
      return;
    case 40:
      error(ERR_ENDOFCOMMENT,lineNo,colNo);
      return;
    default: break;
    }
  }
}

// read keyword until reaching space or other special character
Token* readIdentKeyword(void) {
  int ln = lineNo;
  int cn = colNo;
  int count = 0;
  char buf[MAX_IDENT_LEN+1];
  while (1) {
    switch(state) {
    case 8:
      if (currentCharCode == CHAR_LETTER || currentCharCode == CHAR_DIGIT) {
        if (count > MAX_IDENT_LEN) {
          state = -1;
          error(ERR_IDENTTOOLONG,lineNo,colNo);
          return makeToken(TK_NONE,ln,cn);
        }

        buf[count] = currentChar;
        count++;
        readCharCode();
      } else {
        state = 9;
      }
      break;
    case 9:
      buf[count] = '\0';
      TokenType keywordType = checkKeyword(buf);
      TokenType tokenType = keywordType == TK_NONE ? TK_IDENT : keywordType;
      
      Token *token = makeToken(tokenType, ln,cn);
      strncpy(token->string,buf,count);
      state = 0;
      return token;
    }
  }
}

//read number and make token 
Token* readNumber(void) {
  int ln = lineNo;
  int cn = colNo;
  int count = 0;
  Token *token = makeToken(TK_NUMBER,ln,cn);
  char buf[MAX_IDENT_LEN+1];
  while (1) {
    switch(state) {
    case 10:
      if (currentCharCode == CHAR_DIGIT) {
        if (count > MAX_NUM_LEN) { // MAX_NUM_LEN added in token.h
          state = -1;
          error(ERR_NUMBERTOOLONG,ln,cn); //ERR added in error.c and error.h
          return makeToken(TK_NONE,ln,cn);
        } 
        buf[count] = currentChar;
        readCharCode();
        count++;
      } else {
        state = 11;
      }
      break;
    case 11:
      buf[count] = '\0';
      strncpy(token->string,buf,count);
      token->value = atoi(buf);
      state = 0;
      return token;
    }
  }
}

//reading const char until ' is reached, else raises an error
Token* readConstChar(void) {
  int ln = lineNo;
  int cn = colNo;
  int charValue;

  readCharCode();
  if (currentChar >= 0x20 && currentChar <= 0x7E)
    {
      charValue = currentChar;

      readCharCode();
      if (currentCharCode == CHAR_SINGLEQUOTE)
        {
          readCharCode();
          Token *constCharToken = makeToken(TK_CHAR, ln, cn);
          constCharToken->value = charValue;
          constCharToken->string[0] = charValue;
          constCharToken->string[1] = '\0';
          state = 0;
          return constCharToken;
        }
    }
  state = -1;
  error(ERR_INVALIDCHARCONSTANT, lineNo, colNo);
  return NULL;

}

Token* getToken(void) {
  Token *token;
  int ln;
  int cn;
  if (currentChar == EOF) 
    return makeToken(TK_EOF, lineNo, colNo);

  switch (charCodes[currentChar]) {
  case CHAR_SPACE:
    state = 1;
    skipBlank();
    state = 0;
    return getToken();
  case CHAR_LETTER:
    state = 8;
    return readIdentKeyword();
  case CHAR_DIGIT:
    state = 10;
    return readNumber();
  case CHAR_PLUS: 
    state = 12;
    token = makeToken(SB_PLUS, lineNo, colNo);
    state = 0;
    readCharCode(); 
    return token;
  case CHAR_MINUS: 
    state = 13;
    token = makeToken(SB_MINUS, lineNo, colNo);
    state = 0;
    readCharCode(); 
    return token;
  case CHAR_TIMES: 
    state = 14;
    token = makeToken(SB_TIMES, lineNo, colNo);
    state = 0;
    readCharCode(); 
    return token;
  case CHAR_SLASH: 
    state = 15;
    token = makeToken(SB_SLASH, lineNo, colNo);
    state = 0;
    readCharCode(); 
    return token; 
  case CHAR_EQ:
    state = 16;
    token = makeToken(SB_EQ, lineNo, colNo);
    state = 0;
    readCharCode(); 
    return token;
  case CHAR_LPAR:
    state = 2;
    ln = lineNo;
    cn = colNo;
    readCharCode();
    if (currentCharCode == CHAR_PERIOD) {
      state = 6;
      readCharCode();
      state = 0;
      return makeToken(SB_LSEL,ln,cn);
    } else if (currentCharCode == CHAR_TIMES) {
      //indication for comment
      state = 3;
      readCharCode();
      skipComment();
      return getToken();
    } else {
      state = 0;
      return makeToken(SB_LPAR,ln,cn);
    }
  case CHAR_SINGLEQUOTE:
    return readConstChar();
  case CHAR_LT:
    ln = lineNo;
    cn = colNo;
    state = 25;
    readCharCode();
    if (currentCharCode == CHAR_EQ) {
      readCharCode();
      state = 0;
      return makeToken(SB_LE,ln,cn);
    } else {
      state = 0;
      return makeToken(SB_LT,ln,cn);
    }
  case CHAR_GT:
    ln = lineNo;
    cn = colNo;
    state = 22;
    readCharCode();
    if (currentCharCode == CHAR_EQ) {
      readCharCode();
      state = 0;
      return makeToken(SB_GE,ln,cn);
    } else {
      state = 0;
      return makeToken(SB_GT,ln,cn);
    }
  case CHAR_EXCLAIMATION: 
    ln = lineNo;
    cn = colNo;
    state = 28;
    readCharCode();
    if (currentCharCode == CHAR_EQ) {
      readCharCode();
      state = 0;
      return makeToken(SB_NEQ,ln,cn);
    } else {
      state = -1;
      error(ERR_INVALIDSYMBOL,ln,cn);
      return makeToken(TK_NONE,ln,cn);
    }
  case CHAR_PERIOD:
    ln = lineNo;
    cn = colNo;
    state = 19;
    readCharCode();
    if (currentCharCode == CHAR_RPAR) {
      readCharCode();
      state = 0;
      return makeToken(SB_RSEL,ln,cn);
    } else {
      state = 0;
      return makeToken(SB_PERIOD,ln,cn);
    }
  case CHAR_COLON:
    ln = lineNo;
    cn = colNo;
    state = 31;
    readCharCode();
    if (currentCharCode == CHAR_EQ) {
      readCharCode();
      state = 0;
      return makeToken(SB_ASSIGN,ln,cn);
    } else {
      state = 0;
      return makeToken(SB_COLON,ln,cn);
    }
  case CHAR_COMMA: 
    token = makeToken(SB_COMMA, lineNo, colNo);
    readCharCode();
    return token;

  case CHAR_SEMICOLON:
    token = makeToken(SB_SEMICOLON, lineNo, colNo);
    readCharCode();
    return token;

  case CHAR_RPAR:
    token = makeToken(SB_RPAR, lineNo, colNo);
    readCharCode();
    return token;
  default:
    token = makeToken(TK_NONE, lineNo, colNo);
    error(ERR_INVALIDSYMBOL, lineNo, colNo);
    readChar(); 
    return token;
  }
}


/******************************************************************/

void printToken(Token *token) {
 
  printf("%d-%d:", token->lineNo, token->colNo);

  switch (token->tokenType) {
  case TK_NONE: printf("TK_NONE\n"); break;
  case TK_IDENT: printf("TK_IDENT(%s)\n", token->string); break;
  case TK_NUMBER: printf("TK_NUMBER(%s)\n", token->string); break;
  case TK_CHAR: printf("TK_CHAR(\'%s\')\n", token->string); break;
  case TK_EOF: printf("TK_EOF\n"); break;

  case KW_PROGRAM: printf("KW_PROGRAM\n"); break;
  case KW_CONST: printf("KW_CONST\n"); break;
  case KW_TYPE: printf("KW_TYPE\n"); break;
  case KW_VAR: printf("KW_VAR\n"); break;
  case KW_INTEGER: printf("KW_INTEGER\n"); break;
  case KW_CHAR: printf("KW_CHAR\n"); break;
  case KW_ARRAY: printf("KW_ARRAY\n"); break;
  case KW_OF: printf("KW_OF\n"); break;
  case KW_FUNCTION: printf("KW_FUNCTION\n"); break;
  case KW_PROCEDURE: printf("KW_PROCEDURE\n"); break;
  case KW_BEGIN: printf("KW_BEGIN\n"); break;
  case KW_END: printf("KW_END\n"); break;
  case KW_CALL: printf("KW_CALL\n"); break;
  case KW_IF: printf("KW_IF\n"); break;
  case KW_THEN: printf("KW_THEN\n"); break;
  case KW_ELSE: printf("KW_ELSE\n"); break;
  case KW_WHILE: printf("KW_WHILE\n"); break;
  case KW_DO: printf("KW_DO\n"); break;
  case KW_FOR: printf("KW_FOR\n"); break;
  case KW_TO: printf("KW_TO\n"); break;

  case SB_SEMICOLON: printf("SB_SEMICOLON\n"); break;
  case SB_COLON: printf("SB_COLON\n"); break;
  case SB_PERIOD: printf("SB_PERIOD\n"); break;
  case SB_COMMA: printf("SB_COMMA\n"); break;
  case SB_ASSIGN: printf("SB_ASSIGN\n"); break;
  case SB_EQ: printf("SB_EQ\n"); break;
  case SB_NEQ: printf("SB_NEQ\n"); break;
  case SB_LT: printf("SB_LT\n"); break;
  case SB_LE: printf("SB_LE\n"); break;
  case SB_GT: printf("SB_GT\n"); break;
  case SB_GE: printf("SB_GE\n"); break;
  case SB_PLUS: printf("SB_PLUS\n"); break;
  case SB_MINUS: printf("SB_MINUS\n"); break;
  case SB_TIMES: printf("SB_TIMES\n"); break;
  case SB_SLASH: printf("SB_SLASH\n"); break;
  case SB_LPAR: printf("SB_LPAR\n"); break;
  case SB_RPAR: printf("SB_RPAR\n"); break;
  case SB_LSEL: printf("SB_LSEL\n"); break;
  case SB_RSEL: printf("SB_RSEL\n"); break;
  }
}

int scan(char *fileName) {
  Token *token;

  if (openInputStream(fileName) == IO_ERROR)
    return IO_ERROR;


  currentCharCode = charCodes[currentChar];


  token = getToken();
  while (token->tokenType != TK_EOF) {
    printToken(token);
    free(token);
    token = getToken();
  }

  free(token);
  closeInputStream();
  return IO_SUCCESS;
}

/******************************************************************/

int main(int argc, char *argv[]) {
  if (argc <= 1) {
    printf("scanner: no input file.\n");
    return -1;
  }

  if (scan(argv[1]) == IO_ERROR) {
    printf("Can\'t read input file!\n");
    return -1;
  }
    
  return 0;
}



